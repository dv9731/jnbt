/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Arrays;

public class TagString extends Tag {
	public static final Charset CHARSET = Charset.forName("UTF-8");
	public static final short MAX_LENGTH = Short.MAX_VALUE;
	private String string;
	private TagShort length = new TagShort();
	private byte[] stringBytes;
	
	public TagString(String string) {
		super(TAG_STRING);
		set(string);
	}
	
	public TagString() {
		this("");
	}
	
	public void set(String string) {
		this.string = string;
		byte[] b = string.getBytes(CHARSET);
		if (b.length > MAX_LENGTH) {
			int cut = MAX_LENGTH;
			if ((b[cut - 1] & 0xFF) > 127) { //cutting off at MAX_LENGTH may cut off in a letter
				for (int i = cut - 1; i >= 0; i--) {
					int ub = b[i] & 0xFF;
					if (ub < 192) continue;
					int bytes = Integer.toBinaryString(ub).indexOf("0");
					if (bytes > cut - i) cut = i;
					break;
				}
			}
			b = Arrays.copyOfRange(b, 0, cut);
			this.string = new String(b, CHARSET);
		}
		length.set((short) b.length);
		stringBytes = b;
		updatePayload = true;
	}
	
	public void clear() {
		set("");
	}
	
	public String get() {
		return string;
	}
	
	protected byte[] updatePayload() {
		ByteBuffer b = ByteBuffer.allocate(length.size(byteOrder) + stringBytes.length); //length.size() should always be 2 as long as length is TagShort
		b.put(length.getPayload(byteOrder));
		b.put(stringBytes);
		return payload = b.array();
	}
	
	public TagShort length() {
		return length.clone();
	}
	
	public String toString() {
		return get();
	}
	
	public String toJSON(String indent) {
		StringBuilder sb = new StringBuilder("\"");
		for (char c : get().toCharArray()) {
			switch (c) {
				case '\\': case '"': sb.append('\\').append(c); break;
				case '\b': sb.append("\\b"); break;
				case '\t': sb.append("\\t"); break;
				case '\r': sb.append("\\r"); break;
				case '\n': sb.append("\\n"); break;
				case '\f': sb.append("\\f"); break;
				default:
					if (c < ' ') sb.append(String.format("\\u%04x", (int) c));
					else sb.append(c);
			}
		}
		return sb.append("\"").toString();
	}
	
	public int hashCode() {
		return string.hashCode();
	}
	
	public boolean equals(Object o) {
		if (o instanceof TagString)
			return this == o || this.get().equals(((TagString) o).get());
		return false;
	}
	
	public TagString clone() {
		return new TagString(string);
	}
	
	public static TagString read(DataInput in) throws IOException {
		TagShort len = TagShort.read(in);
		byte[] b = new byte[len.get()];
		in.readFully(b);
		return new TagString(new String(b, CHARSET));
	}
}
