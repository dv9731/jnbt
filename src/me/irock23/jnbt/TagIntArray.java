/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class TagIntArray extends Tag {
	private int[] ints;
	private TagInt length = new TagInt();

	public TagIntArray(int[] ints) {
		super(TAG_INT_ARRAY);
		set(ints);
	}
	
	public void set(int[] ints) {
		this.ints = ints;
		length.set(ints.length);
		updatePayload = true;
	}

	public void clear() {
		set(new int[0]);
	}
	
	public int[] get() {
		return ints;
	}
	
	protected byte[] updatePayload() {
		ByteBuffer buf = ByteBuffer.allocate(length.size(byteOrder) + ints.length*4).order(byteOrder);
		buf.putInt(length.get());
		for (int i : ints) buf.putInt(i);
		return payload = buf.array();
	}
	
	public TagInt length() {
		return length.clone();
	}
	
	public String toString() {
		return Arrays.toString(get());
	}
	
	public String toJSON(String indent) {
		return "i" + Arrays.toString(get());
	}
	
	public TagIntArray clone() {
		return new TagIntArray(Arrays.copyOf(ints, ints.length));
	}

	public int hashCode() {
		return Arrays.hashCode(ints);
	}

	public boolean equals(Object o) {
		return this == o || o instanceof TagIntArray && Arrays.equals(this.get(), ((TagIntArray) o).get());
	}
	
	public static TagIntArray read(DataInput in) throws IOException {
		TagInt len = TagInt.read(in);
		int[] ints = new int[len.get()];
		for (int i = 0; i < ints.length; i++) ints[i] = in.readInt();
		return new TagIntArray(ints);
	}

}
