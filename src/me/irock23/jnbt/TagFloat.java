/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TagFloat extends Tag {
	private float f;
	
	public TagFloat(float f) {
		super(TAG_FLOAT);
		this.f = f;
	}
	
	public TagFloat() {
		this(0f);
	}
	
	public TagFloat set(float f) {
		this.f = f;
		updatePayload = true;
		return this;
	}
	
	public TagFloat add(float f) {
		return set(this.f + f);
	}
	
	public TagFloat subtract(float f) {
		return set(this.f - f);
	}
	
	public void clear() {
		set(0f);
	}
	
	public float get() {
		return f;
	}
	
	protected byte[] updatePayload() {
		return payload = ByteBuffer.allocate(4).order(byteOrder).putFloat(f).array();
	}
	
	public String toString() {
		return String.valueOf(get());
	}
	
	public String toJSON(String indent) {
		return get() + "f";
	}
	
	public TagFloat clone() {
		return new TagFloat(f);
	}
	
	public int hashCode() {
		return Float.hashCode(f);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagFloat && this.get() == ((TagFloat) o).get();
	}
	
	public static TagFloat read(DataInput in) throws IOException {
		return new TagFloat(in.readFloat());
	}
}
