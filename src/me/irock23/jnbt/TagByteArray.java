/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.util.Arrays;

public class TagByteArray extends Tag {
	private byte[] bytes;
	private TagInt length = new TagInt();
	
	public TagByteArray(byte[] bytes) {
		super(TAG_BYTE_ARRAY);
		set(bytes);
	}
	
	public TagByteArray() {
		this(new byte[0]);
	}
	
	public void set(byte[] bytes) {
		this.bytes = bytes;
		length.set(bytes.length);
		updatePayload = true;
	}
	
	public void clear() {
		set(new byte[0]);
	}
	
	public byte[] get() {
		return bytes;
	}
	
	protected byte[] updatePayload() {
		payload = Arrays.copyOf(length.getPayload(byteOrder), length.size(byteOrder) + bytes.length);
		System.arraycopy(bytes, 0, payload, length.size(byteOrder), bytes.length);
		return payload;
	}
	
	public TagInt length() {
		return length.clone();
	}
	
	public String toString() {
		return Arrays.toString(get());
	}
	
	public String toJSON(String indent) {
		String json = "b" + Arrays.toString(get());
		if (indent == null) return json.replaceAll("\\s+", "");
		return json;
	}
	
	public TagByteArray clone() {
		return new TagByteArray(Arrays.copyOf(bytes, bytes.length));
	}
	
	public int hashCode() {
		return Arrays.hashCode(bytes);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagByteArray && Arrays.equals(this.get(), ((TagByteArray) o).get());
	}
	
	public static TagByteArray read(DataInput in) throws IOException {
		TagInt len = TagInt.read(in);
		byte[] b = new byte[len.get()];
		in.readFully(b);
		return new TagByteArray(b);
	}
}
