/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.Closeable;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteOrder;
import java.util.zip.GZIPOutputStream;

public class NBTOutputStream implements Closeable {
	protected final DataOutput out;
	protected DataOutputStream stream;
	protected final boolean compressed;
	protected ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
	
	public NBTOutputStream(OutputStream out, boolean compressed) throws IOException {
		this.out = stream = new DataOutputStream(compressed ? new GZIPOutputStream(out) : out);
		this.compressed = compressed;
	}
	
	public NBTOutputStream(OutputStream out) throws IOException {
		this(out, true);
	}
	
	protected NBTOutputStream(DataOutput out, boolean compressed) {
		this.out = out;
		this.compressed = compressed;
	}
	
	public void writeNBT(NBT tag) throws IOException {
		ByteOrder bo = tag.getByteOrder();
		out.write(tag.getBytes(byteOrder));
		tag.setByteOrder(bo);
	}
	
	public boolean isStandard() {
		return compressed && byteOrder == ByteOrder.BIG_ENDIAN;
	}

	@Override
	public void close() throws IOException {
		if (stream != null) stream.close();
	}
}
