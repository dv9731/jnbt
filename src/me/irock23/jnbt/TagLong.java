/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TagLong extends Tag {
	private long l;
	
	public TagLong(long l) {
		super(TAG_LONG);
		this.l = l;
	}
	
	public TagLong() {
		this(0L);
	}
	
	public TagLong set(long l) {
		this.l = l;
		updatePayload = true;
		return this;
	}
	
	public TagLong add(long l) {
		return set(this.l + l);
	}
	
	public TagLong subtract(long l) {
		return set(this.l - l);
	}
	
	public void clear() {
		set(0L);
	}
	
	public long get() {
		return l;
	}
	
	protected byte[] updatePayload() {
		return payload = ByteBuffer.allocate(8).order(byteOrder).putLong(l).array();
	}
	
	public String toString() {
		return String.valueOf(get());
	}
	
	public String toJSON(String indent) {
		return get() + "l";
	}
	
	public TagLong clone() {
		return new TagLong(l);
	}
	
	public int hashCode() {
		return Long.hashCode(l);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagLong && this.get() == ((TagLong) o).get();
	}
	
	public static TagLong read(DataInput in) throws IOException {
		return new TagLong(in.readLong());
	}
}
