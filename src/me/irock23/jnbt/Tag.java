/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteOrder;

/**
 * A class representing a single explicitly-identified tag as part of the NBT specification
 * @author David Vick
 */
public abstract class Tag implements Cloneable {
	public static final String[] NAMES = new String[] {"TAG_End", "TAG_Byte", "TAG_Short", "TAG_Int", "TAG_Long", "TAG_Float", "TAG_Double", "TAG_Byte_Array", "TAG_String", "TAG_List", "TAG_Compound", "TAG_Int_Array"};
	public static final int DEFINED_TAGS = NAMES.length;
	public static final byte TAG_END = 0;
	public static final byte TAG_BYTE = 1;
	public static final byte TAG_SHORT = 2;
	public static final byte TAG_INT = 3;
	public static final byte TAG_LONG = 4;
	public static final byte TAG_FLOAT = 5;
	public static final byte TAG_DOUBLE = 6;
	public static final byte TAG_BYTE_ARRAY = 7;
	public static final byte TAG_STRING = 8;
	public static final byte TAG_LIST = 9;
	public static final byte TAG_COMPOUND = 10;
	public static final byte TAG_INT_ARRAY = 11;
	/** 
	 * The default {@link ByteOrder} to use when generating the payload.
	 * This is initially set to {@link ByteOrder#BIG_ENDIAN} as defined
	 * by the standard NBT specification. Modified versions may use a
	 * little endian byte order.
	 */
	protected static ByteOrder defaultByteOrder = ByteOrder.BIG_ENDIAN;
	
	/** The {@link ByteOrder} to use when generating the payload */
	protected ByteOrder byteOrder = defaultByteOrder;
	/** The type of this {@link Tag} */
	protected final byte type;
	/** The raw bytes representing this {@link Tag} */
	protected byte[] payload;
	protected boolean updatePayload = false;
	
	Tag(byte type, byte[] payload) {
		this.type = type;
		this.payload = payload;
	}
	
	Tag(byte type) {
		this(type, new byte[0]);
		updatePayload = true;
	}
	
	/**
	 * Returns the {@link #type} of this {@link Tag}
	 * @return {@link #type} of this {@link Tag} instance
	 */
	public final byte getType() {
		return type;
	}
	
	/**
	 * Returns the payload of this {@link Tag}, or updates the payload if changed and then returns
	 * @return the {@link #payload} of this {@link Tag} instance
	 */
	public byte[] getPayload() {
		if (updatePayload) {
			updatePayload = false;
			return updatePayload();
		}
		return payload;
	}
	
	/**
	 * Returns the {@link #payload} of this {@link Tag}, or updates the {@link #payload} if changed or
	 * different {@link ByteOrder}. {@link #byteOrder} will be set to {@code bo}.
	 * @param bo the {@link ByteOrder} for this {@link Tag} to use
	 * @return the {@link #payload} of this {@link Tag} instance
	 */
	public byte[] getPayload(ByteOrder bo) {
		if (updatePayload || bo != null && !bo.equals(byteOrder)) {
			byteOrder = bo;
			updatePayload = false;
			return updatePayload();
		}
		return payload;
	}
	
	protected byte[] updatePayload() {
		return payload;
	}
	
	/**
	 * Sets the {@link #byteOrder} to {@code bo}
	 * @param bo the {@link ByteOrder} for this {@link Tag} to use
	 */
	public final void setByteOrder(ByteOrder bo) {
		if (bo != null && !bo.equals(byteOrder)) {
			byteOrder = bo;
			updatePayload = true;
		}
	}
	
	/**
	 * Returns the {@link ByteOrder} this {@link Tag} is using
	 * @return the {@link #byteOrder} of this {@link Tag} instance
	 */
	public final ByteOrder getByteOrder() {
		return byteOrder;
	}
	
	/**
	 * Returns the number of bytes in the {@link #payload} of this {@link Tag}.
	 * Generates the {@link #payload} if necessary.
	 * @return the length of this {@link Tag}'s {@link #payload}
	 */
	public final int size() {
		return getPayload().length;
	}
	
	/**
	 * Returns the number of bytes in the {@link #payload} of this {@link Tag}.
	 * Sets the {@link #byteOrder} to {@code bo}. Generates the {@link #payload} if necessary.
	 * @param bo the {@link ByteOrder} for this {@link Tag} to use
	 * @return the length of this {@link Tag}'s {@link #payload}
	 */
	public final int size(ByteOrder bo) {
		return getPayload(bo).length;
	}
	
	public abstract void clear();
	
	/**
	 * Returns the friendly type name of this {@link Tag}
	 * @return this {@link Tag}'s {@link #type}'s name
	 */
	public final String getTypeName() {
		return Tag.getTypeName(type);
	}
	
	/** @return a {@link String} representing this {@link Tag}'s {@link #type} and value */
	public final String print() {
		return getTypeName() + ": " + toString();
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagEnd} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagEnd}, {@code false} otherwise
	 */
	public final boolean isTagEnd() {
		return this instanceof TagEnd;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagByte} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagByte}, {@code false} otherwise
	 */
	public final boolean isTagByte() {
		return this instanceof TagByte;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagShort} instance
	 * @return {@code true} {@link Tag} is a {@link TagShort}, {@code false} otherwise
	 */
	public final boolean isTagShort() {
		return this instanceof TagShort;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagInt} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagInt}, {@code false} otherwise
	 */
	public final boolean isTagInt() {
		return this instanceof TagInt;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagLong} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagLong}, {@code false} otherwise
	 */
	public final boolean isTagLong() {
		return this instanceof TagLong;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagFloat} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagFloat}, {@code false} otherwise
	 */
	public final boolean isTagFloat() {
		return this instanceof TagFloat;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagDouble} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagDouble}, {@code false} otherwise
	 */
	public final boolean isTagDouble() {
		return this instanceof TagDouble;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagByteArray} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagByteArray}, {@code false} otherwise
	 */
	public final boolean isTagByteArray() {
		return this instanceof TagByteArray;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagString} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagString}, {@code false} otherwise
	 */
	public final boolean isTagString() {
		return this instanceof TagString;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagList} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagList}, {@code false} otherwise
	 */
	public final boolean isTagList() {
		return this instanceof TagList;
	}
	
	/**
	 * Determines if this {@link Tag} is a {@link TagCompound} instance
	 * @return {@code true} if this {@link Tag} is a {@link TagCompound}, {@code false} otherwise
	 */
	public final boolean isTagCompound() {
		return this instanceof TagCompound;
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagEnd}
	 * @return this {@link Tag} as a {@link TagEnd}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagEnd getAsTagEnd() {
		if (isTagEnd()) return (TagEnd) this;
		throw new IllegalStateException("This is not a TAG_End");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagByte}
	 * @return this {@link Tag} as a {@link TagByte}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagByte getAsTagByte() {
		if (isTagByte()) return (TagByte) this;
		throw new IllegalStateException("This is not a TAG_Byte");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagShort}
	 * @return this {@link Tag} as a {@link TagShort}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagShort getAsTagShort() {
		if (isTagShort()) return (TagShort) this;
		throw new IllegalStateException("This is not a TAG_Short");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagInt}
	 * @return this {@link Tag} as a {@link TagInt}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagInt getAsTagInt() {
		if (isTagInt()) return (TagInt) this;
		throw new IllegalStateException("This is not a TAG_Int");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagLong}
	 * @return this {@link Tag} as a {@link TagLong}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagLong getAsTagLong() {
		if (isTagLong()) return (TagLong) this;
		throw new IllegalStateException("This is not a TAG_Long");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagFloat}
	 * @return this {@link Tag} as a {@link TagFloat}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagFloat getAsTagFloat() {
		if (isTagFloat()) return (TagFloat) this;
		throw new IllegalStateException("This is not a TAG_Float");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagDouble}
	 * @return this {@link Tag} as a {@link TagDouble}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagDouble getAsTagDouble() {
		if (isTagDouble()) return (TagDouble) this;
		throw new IllegalStateException("This is not a TAG_Double");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagByteArray}
	 * @return this {@link Tag} as a {@link TagByteArray}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagByteArray getAsTagByteArray() {
		if (isTagByteArray()) return (TagByteArray) this;
		throw new IllegalStateException("This is not a TAG_Byte_Array");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagString}
	 * @return this {@link Tag} as a {@link TagString}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagString getAsTagString() {
		if (isTagString()) return (TagString) this;
		throw new IllegalStateException("This is not a TAG_String");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagList}
	 * @return this {@link Tag} as a {@link TagList}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagList getAsTagList() {
		if (isTagList()) return (TagList) this;
		throw new IllegalStateException("This is not a TAG_List");
	}
	
	/**
	 * Gets this {@link Tag} as a {@link TagCompound}
	 * @return this {@link Tag} as a {@link TagCompound}
	 * @throws IllegalStateException if this {@link Tag} is another type
	 */
	public final TagCompound getAsTagCompound() {
		if (isTagCompound()) return (TagCompound) this;
		throw new IllegalStateException("This is not a TAG_Compound");
	}
	
	public abstract String toString();
	
	public abstract String toJSON(String indent);
	
	public String toJSON() {
		return toJSON(null);
	}

	public abstract int hashCode();
	
	/**
	 * Compares this {@link Tag} to {@link Object} {@code o}.
	 * @param o {@link Object} to compare this {@link Tag} to
	 */
	public abstract boolean equals(Object o);
	
	public abstract Tag clone();
	
	public static final String getTypeName(byte type) {
		return NAMES[type];
	}
	
	public static final void setDefaultByteOrder(ByteOrder bo) {
		if (bo != null) defaultByteOrder = bo;
	}
	
	public static final ByteOrder getDefaultByteOrder() {
		return defaultByteOrder;
	}
	
	/**
	 * Reads a {@link Tag} from a {@link DataInput} of the specified {@link #type}.
	 * An {@link IOException} will be thrown if a {@code TAG_End} is not found at a depth greater than 0,
	 * an invalid {@link #type} is found, or any other IO error occurs.
	 * @param in {@link DataInput} to read from
	 * @param type the {@link #type} of {@link Tag} to read
	 * @param depth the depth of {@link Tag} to read
	 * @return {@link Tag} with {@link #type} {@code type}
	 * @throws IOException
	 */
	public static Tag read(DataInput in, byte type, int depth) throws IOException {
		switch (type) {
			case TAG_END:
				if (depth > 0) return new TagEnd();
				else throw new IOException("TAG_End found without a preceding TAG_Compound to match");
			case TAG_BYTE:
				return TagByte.read(in);
			case TAG_SHORT:
				return TagShort.read(in);
			case TAG_INT:
				return TagInt.read(in);
			case TAG_LONG:
				return TagLong.read(in);
			case TAG_FLOAT:
				return TagFloat.read(in);
			case TAG_DOUBLE:
				return TagDouble.read(in);
			case TAG_BYTE_ARRAY:
				return TagByteArray.read(in);
			case TAG_STRING:
				return TagString.read(in);
			case TAG_LIST:
				return TagList.read(in, depth);
			case TAG_COMPOUND:
				return TagCompound.read(in, depth);
			case TAG_INT_ARRAY:
				return TagIntArray.read(in);
			default:
				throw new IOException("Invalid tag type: " + type);
		}
	}
	
	/**
	 * Convert a specific, lenient JSON format into a {@link Tag} object
	 * <br><b>Not yet implemented!</b>
	 * @param json JSON data to be converted
	 * @return a {@link Tag} instance representing the JSON data
	 */
	public static Tag fromJSON(String json) {
		return null;
	}
}
