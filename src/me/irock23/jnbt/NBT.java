/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class NBT implements Cloneable {
	/** The NBT version implemented */
	public static final String VERSION = "19133";
	protected final TagString name;
	protected final Tag tag;
	
	public NBT(TagString name, Tag tag) {
		this.name = name;
		this.tag = tag;
		name.setByteOrder(tag.getByteOrder());
	}
	
	public NBT(String name, Tag tag) {
		this(new TagString(name), tag);
	}
	
	public void setName(String name) {
		this.name.set(name);
	}
	
	public void setName(TagString name) {
		setName(name.get());
	}
	
	public TagString getName() {
		return name;
	}
	
	public Tag getTag() {
		return tag;
	}
	
	public byte[] getBytes(ByteOrder byteOrder) {
		ByteBuffer b = ByteBuffer.allocate(size(byteOrder));
		b.put(tag.getType());
		b.put(name.getPayload(byteOrder));
		b.put(tag.getPayload(byteOrder));
		return b.array();
	}
	
	public byte[] getBytes() {
		ByteBuffer b = ByteBuffer.allocate(size());
		b.put(tag.getType());
		b.put(name.getPayload());
		b.put(tag.getPayload());
		return b.array();
	}
	
	public void setByteOrder(ByteOrder bo) {
		if (bo != null) {
			tag.setByteOrder(bo);
			name.setByteOrder(bo);
		}
	}
	
	public ByteOrder getByteOrder() {
		return tag.getByteOrder();
	}
	
	/**
	 * Updates the {@link Tag#byteOrder} of the {@link #name} and the {@link #tag}
	 * and calculates the size of this {@link NBT}
	 * @param byteOrder {@link Tag#byteOrder} for {@link #name} and {@link #tag}
	 * @return the size of the raw data representing this {@link NBT}
	 */
	public int size(ByteOrder byteOrder) {
		return 1 + name.size(byteOrder) + tag.size(byteOrder);
	}
	
	/**
	 * Calculates the size of this {@link NBT}
	 * @return the size of the raw data representing this {@link NBT}
	 */
	public int size() {
		return 1 + name.size() + tag.size();
	}
	
	public byte getType() {
		return tag.getType();
	}
	
	public String toString() {
		return tag.getTypeName() + "(\"" + name + "\"): " + tag;
	}
	
	public String toJSON(String indent) {
		return name.toJSON() + ":" + (indent != null ? " " : "") + tag.toJSON(indent);
	}
	
	public String toJSON() {
		return toJSON(null);
	}
	
	public String print() {
		return toString();
	}
	
	public int hashCode() {
		return name.hashCode() + tag.hashCode();
	}
	
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o instanceof NBT) {
			NBT t = (NBT) o;
			return this.getName().equals(t.getName()) && this.getTag().equals(t.getTag());
		}
		return false;
	}
	
	public NBT clone() {
		return new NBT(name.clone(), tag.clone());
	}
	
	public static NBT read(DataInput in, int depth) throws IOException {
		byte type = in.readByte();
		TagString name = null;
		if (type != Tag.TAG_END) name = TagString.read(in);
		else name = new TagString("");
		return new NBT(name, Tag.read(in, type, depth));
	}
	
	public static NBT read(DataInput in) throws IOException {
		return read(in, 0);
	}
}
