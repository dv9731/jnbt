/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TagCompound extends Tag {
	private Map<TagString, NBT> tags = new HashMap<TagString, NBT>();
	
	public TagCompound() {
		super(TAG_COMPOUND);
	}
	
	public TagCompound(List<NBT> tags) {
		this();
		add(tags);
	}
	
	public TagCompound(Map<TagString, NBT> tags) {
		this();
		set(tags);
	}
	
	public TagCompound set(List<NBT> tags) {
		this.tags = new HashMap<TagString, NBT>();
		return add(tags);
	}
	
	public TagCompound set(Map<TagString, NBT> tags) {
		if (tags != null) this.tags = tags;
		return this;
	}
	
	public TagCompound add(List<NBT> tags) {
		for (NBT t : tags) this.tags.put(t.getName(), t);
		return this;
	}
	
	public TagCompound add(Map<TagString, NBT> tags) {
		return add(new ArrayList<NBT>(tags.values()));
	}
	
	public TagCompound add(NBT tag) {
		tags.put(tag.getName(), tag);
		return this;
	}
	
	public TagCompound put(TagString name, Tag tag) {
		return add(new NBT(name, tag));
	}
	
	public TagCompound put(String name, Tag tag) {
		return put(new TagString(name), tag);
	}
	
	public NBT remove(TagString name) {
		return tags.remove(name);
	}
	
	public NBT remove(String name) {
		return remove(new TagString(name));
	}
	
	public void clear() {
		tags.clear();
	}
	
	public byte[] getPayload() {
		return updatePayload(); //Any tag's payload might have changed
	}
	
	protected byte[] updatePayload() {
		int totalSize = 0;
		for (NBT t : tags.values()) totalSize += t.size(byteOrder);
		ByteBuffer b = ByteBuffer.allocate(totalSize + 1); //extra byte for TAG_End (0)
		for (NBT t : tags.values()) b.put(t.getBytes(byteOrder));
		return payload = b.array();
	}
	
	public Map<TagString, NBT> get() {
		return tags;
	}
	
	public NBT get(TagString name) {
		return tags.get(name);
	}
	
	public NBT get(String name) {
		return get(new TagString(name));
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		for (NBT t : tags.values())
			sb.append("\n\t").append(t.print().replace("\n", "\n\t"));
		if (tags.size() > 0) sb.append("\n");
		return sb.append("}").toString();
	}
	
	public String toJSON(String indent) {
		boolean pretty = indent != null;
		StringBuilder sb = new StringBuilder("{");
		for (NBT t : tags.values()) {
			if (pretty) sb.append("\n").append(indent).append(t.toJSON(indent).replace("\n", "\n" + indent));
			else sb.append(t.toJSON(indent));
			sb.append(",");
		}
		if (sb.charAt(sb.length() - 1) == ',') sb.deleteCharAt(sb.length() - 1);
		if (pretty && tags.size() > 0) sb.append("\n");
		return sb.append("}").toString();
	}
	
	public int hashCode() {
		return get().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o instanceof TagCompound)
			return this == o || this.get().equals(((TagCompound) o).get());
		return false;
	}
	
	public TagCompound clone() {
		Map<TagString, NBT> newMap = new HashMap<TagString, NBT>();
		for (NBT t : tags.values()) {
			newMap.put(t.getName().clone(), t.clone());
		}
		return new TagCompound(newMap);
	}
	
	public static TagCompound read(DataInput in, int depth) throws IOException {
		Map<TagString, NBT> tagMap = new HashMap<TagString, NBT>();
		while (true) {
			NBT t = NBT.read(in, depth + 1);
			if (t.getTag().isTagEnd()) break;
			else tagMap.put(t.getName(), t);
		}
		return new TagCompound(tagMap);
	}
}
