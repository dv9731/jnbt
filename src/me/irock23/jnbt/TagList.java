/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TagList extends Tag {
	private final byte listType;
	private List<Tag> tags;
	private TagInt length = new TagInt();
	
	public TagList(byte type, List<Tag> tags) {
		super(TAG_LIST);
		listType = type;
		if (listType == TAG_END)
			throw new IllegalStateException("A TAG_List cannot be made up of TAG_Ends");
		set(tags);
	}
	
	public TagList(byte type, Tag[] tags) {
		this(type, Arrays.asList(tags));
	}
	
	public TagList(byte type) {
		this(type, new ArrayList<Tag>());
	}
	
	public byte getListType() {
		return listType;
	}
	
	public TagList set(List<Tag> tags) {
		this.tags = new ArrayList<Tag>();
		return add(tags);
	}
	
	public TagList set(Tag[] tags) {
		return set(Arrays.asList(tags));
	}
	
	public TagList add(List<Tag> tags) {
		for (Tag t : tags)
			if (t.getType() == listType)
				this.tags.add(t);
		length.set(this.tags.size());
		return this;
	}
	
	public TagList add(Tag[] tags) {
		return add(Arrays.asList(tags));
	}
	
	public TagList add(Tag tag) {
		return add(Arrays.asList(tag));
	}
	
	public TagList add(int index, Tag tag) {
		tags.add(index, tag);
		length.set(this.tags.size());
		return this;
	}
	
	public void clear() {
		tags.clear();
		length.set(this.tags.size());
	}
	
	public List<Tag> get() {
		return tags;
	}
	
	public int indexOf(Tag t) {
		return tags.indexOf(t);
	}
	
	public Tag remove(int index) {
		Tag t = tags.remove(index);
		length.set(this.tags.size());
		return t;
	}
	
	public boolean remove(Tag tag) {
		boolean r = tags.remove(tag);
		length.set(this.tags.size());
		return r;
	}
	
	public byte[] getPayload() {
		return updatePayload();
	}
	
	protected byte[] updatePayload() {
		length.set(this.tags.size());
		int totalSize = 0;
		for (Tag t : tags) totalSize += t.size(byteOrder);
		ByteBuffer b = ByteBuffer.allocate(1 + length.size(byteOrder) + totalSize);
		b.put(listType);
		b.put(length.getPayload(byteOrder));
		for (Tag t : tags) b.put(t.getPayload(byteOrder));
		return payload = b.array();
	}
	
	public TagInt updateLength() {
		return length.set(tags.size()).clone();
	}
	
	public TagInt length() {
		return length.clone();
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder("{");
		for (Tag t : tags)
			sb.append("\n\t" + t.print().replace("\n", "\n\t"));
		sb.append("\n}");
		return sb.toString();
	}
	
	public String toJSON(String indent) {
		boolean pretty = indent != null;
		StringBuilder sb = new StringBuilder("[");
		for (Tag t : tags) {
			sb.append(t.toJSON(indent));
			sb.append(",");
			if (pretty) sb.append(" ");
		}
		int last = sb.length() - 1;
		if (sb.charAt(last) == ',') sb.deleteCharAt(last);
		else if (sb.charAt(last) == ' ') sb.delete(last - 1, last + 1);
		if (pretty && tags.size() > 0) sb.append("\n");
		return sb.append("]").toString();
	}
	
	public int hashCode() {
		return get().hashCode();
	}
	
	public boolean equals(Object o) {
		if (o instanceof TagList)
			return this == o || this.get().equals(((TagList) o).get());
		return false;
	}
	
	public TagList clone() {
		List<Tag> newList = new ArrayList<Tag>();
		for (Tag t : tags) {
			newList.add(t.clone());
		}
		return new TagList(listType, newList);
	}
	
	public static TagList read(DataInput in, int depth) throws IOException {
		byte listType = in.readByte();
		if (listType == TAG_END)
			throw new IOException("A TAG_List cannot be made up of TAG_Ends");
		TagInt len = TagInt.read(in);
		List<Tag> tagList = new ArrayList<Tag>();
		for (int i = 0; i < len.get(); i++) {
			tagList.add(Tag.read(in, listType, depth + 1));
		}
		return new TagList(listType, tagList);
	}
}
