package me.irock23.jnbt;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Test {
	public static void main(String[] args) throws Exception {
		NBTInputStream in = new NBTInputStream(new FileInputStream(new File("bigtest.nbt")));
		NBT bigtest = in.readNBT();
		in.close();
		System.out.println(bigtest.print());
		if (args.length > 0 && args[0] == "writetest") {
			NBTOutputStream out = new NBTOutputStream(new FileOutputStream(new File("bigtest-out.nbt")));
			out.writeNBT(bigtest);
			out.close();
			System.out.println("----------------------------------------------");
			in = new NBTInputStream(new FileInputStream(new File("bigtest-out.nbt")));
			NBT bigtestOut = in.readNBT();
			in.close();
			System.out.println(bigtestOut.print());
		}
	}
}
