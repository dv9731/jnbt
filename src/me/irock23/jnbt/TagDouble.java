/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TagDouble extends Tag {
	private double d;
	
	public TagDouble(double d) {
		super(TAG_DOUBLE);
		this.d = d;
	}
	
	public TagDouble() {
		this(0d);
	}
	
	public TagDouble set(double d) {
		this.d = d;
		updatePayload = true;
		return this;
	}
	
	public TagDouble add(double d) {
		return set(this.d + d);
	}
	
	public TagDouble subtract(double d) {
		return set(this.d - d);
	}
	
	public void clear() {
		set(0d);
	}
	
	public double get() {
		return d;
	}
	
	protected byte[] updatePayload() {
		return payload = ByteBuffer.allocate(8).order(byteOrder).putDouble(d).array();
	}
	
	public String toString() {
		return String.valueOf(get());
	}
	
	public String toJSON(String indent) {
		return get() + "d";
	}
	
	public TagDouble clone() {
		return new TagDouble(d);
	}
	
	public int hashCode() {
		return Double.hashCode(d);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagDouble && this.get() == ((TagDouble) o).get();
	}
	
	public static TagDouble read(DataInput in) throws IOException {
		return new TagDouble(in.readDouble());
	}
}
