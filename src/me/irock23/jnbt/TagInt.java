/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TagInt extends Tag {
	private int i;
	
	public TagInt(int i) {
		super(TAG_INT);
		this.i = i;
	}
	
	public TagInt() {
		this(0);
	}
	
	public TagInt set(int i) {
		this.i = i;
		updatePayload = true;
		return this;
	}
	
	public TagInt add(int i) {
		return set(this.i + i);
	}
	
	public TagInt subtract(int i) {
		return set(this.i - i);
	}
	
	public void clear() {
		set(0);
	}
	
	public int get() {
		return i;
	}
	
	protected byte[] updatePayload() {
		return payload = ByteBuffer.allocate(4).order(byteOrder).putInt(i).array();
	}
	
	public String toString() {
		return String.valueOf(get());
	}
	
	public String toJSON(String indent) {
		return String.valueOf(get());
	}
	
	public TagInt clone() {
		return new TagInt(i);
	}
	
	public int hashCode() {
		return Integer.hashCode(i);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagInt && this.get() == ((TagInt) o).get();
	}
	
	public static TagInt read(DataInput in) throws IOException {
		return new TagInt(in.readInt());
	}
}
