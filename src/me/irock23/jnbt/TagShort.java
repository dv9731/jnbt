/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.DataInput;
import java.io.IOException;
import java.nio.ByteBuffer;

public class TagShort extends Tag {
	private short s;
	
	public TagShort(short s) {
		super(TAG_SHORT);
		this.s = s;
	}
	
	public TagShort() {
		this((short) 0);
	}
	
	public TagShort set(short s) {
		this.s = s;
		updatePayload = true;
		return this;
	}
	
	public TagShort add(short s) {
		return set((short) (this.s + s));
	}
	
	public TagShort subtract(short s) {
		return set((short) (this.s - s));
	}
	
	public void clear() {
		set((short) 0);
	}
	
	public short get() {
		return s;
	}
	
	protected byte[] updatePayload() {
		return payload = ByteBuffer.allocate(2).order(byteOrder).putShort(s).array();
	}
	
	public String toString() {
		return String.valueOf(get());
	}
	
	public String toJSON(String indent) {
		return get() + "s";
	}
	
	public TagShort clone() {
		return new TagShort(s);
	}
	
	public int hashCode() {
		return Short.hashCode(s);
	}
	
	public boolean equals(Object o) {
		return this == o || o instanceof TagShort && this.get() == ((TagShort) o).get();
	}
	
	public static TagShort read(DataInput in) throws IOException {
		return new TagShort(in.readShort());
	}
}
