/**
 * Copyright 2014 David Vick
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.irock23.jnbt;

import java.io.Closeable;
import java.io.DataInput;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteOrder;
import java.util.zip.GZIPInputStream;

public class NBTInputStream implements Closeable {
	protected final DataInput in;
	protected DataInputStream stream;
	protected final boolean compressed;
	protected ByteOrder byteOrder = ByteOrder.BIG_ENDIAN;
	
	public NBTInputStream(InputStream in, boolean compressed) throws IOException {
		this.in = stream = new DataInputStream(compressed ? new GZIPInputStream(in) : in);
		this.compressed = compressed;
	}
	
	public NBTInputStream(InputStream in) throws IOException {
		this(in, true);
	}
	
	protected NBTInputStream(DataInput in, boolean compressed) {
		this.in = in;
		this.compressed = compressed;
	}
	
	public NBT readNBT() throws IOException {
		return NBT.read(in, 0);
	}
	
	public boolean isStandard() {
		return compressed && byteOrder == ByteOrder.BIG_ENDIAN;
	}

	@Override
	public void close() throws IOException {
		if (stream != null) stream.close();
	}
}
